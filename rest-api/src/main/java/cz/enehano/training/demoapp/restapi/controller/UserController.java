package cz.enehano.training.demoapp.restapi.controller;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.service.UserServiceImpl;

import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class UserController {
    
    @Autowired
    private UserServiceImpl userServiceImpl;
    
    @GetMapping(value="/users")
    public Stream<UserDto> getAllUsers() {
        return userServiceImpl.getAllUsers();
    }
    
    @GetMapping(value="/users/{id}")
    public UserDto getUser(@PathVariable Long id) {
        return userServiceImpl.getUser(id);
    }

    @PostMapping(value="/users")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto createUser(@RequestBody UserDto dto) {
        return userServiceImpl.createUser(dto);
    }

    @PutMapping(value="/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto updateUser(@RequestBody UserDto dto,@PathVariable Long id) {
        return userServiceImpl.updateUser(dto, id);
    }

    @DeleteMapping(value="/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@PathVariable Long id) {
        userServiceImpl.deleteUser(id);
    }


}
