package cz.enehano.training.demoapp.restapi.dto;

import lombok.Data;

@Data
public class UserDto {

    private String name;
    private String surname;
    private String email;
    private String password;
    private String phoneNumber;
    private String createdBy;

    // you can add another attributes like address, email, phone etc.
}
