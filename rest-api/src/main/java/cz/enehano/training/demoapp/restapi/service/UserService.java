/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import java.util.List;
import java.util.stream.Stream;

/**
 *
 * @author Kluzamic
 */
public interface UserService {
    public Stream<UserDto> getAllUsers();

    public UserDto getUser(Long id);
    
    public UserDto createUser(UserDto dto);

    public UserDto updateUser(UserDto dto, Long id);

    public void deleteUser(Long id);
}
