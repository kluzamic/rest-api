/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.enehano.training.demoapp.restapi.service;

import cz.enehano.training.demoapp.restapi.dto.UserDto;
import cz.enehano.training.demoapp.restapi.model.User;
import cz.enehano.training.demoapp.restapi.repository.UserRepository;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

/**
 *
 * @author Kluzamic
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Override
    public Stream<UserDto> getAllUsers() {
        // sort, and map to DTO
        return userRepository.findAll().stream().sorted(new Comparator<User>(){
            public int compare(User a, User b) {
            return a.getLastName().compareTo(b.getLastName());
            }
        }).map(user -> modelMapper.map(user, UserDto.class));
    }

    @Override
    public UserDto getUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(()-> new NotFoundException(id));
        return modelMapper.map(user, UserDto.class);
    }

    @Override
    public UserDto createUser(UserDto dto) {
        User user = modelMapper.map(dto, User.class);
        userRepository.save(user);
        return dto;
    }

    @Override
    public UserDto updateUser(UserDto dto, Long id) {
        User user = userRepository.findById(id).orElseThrow(()-> new NotFoundException(id));
        
        user = modelMapper.map(dto, User.class);
        
        return dto;
    }

    @Override
    public void deleteUser(Long id) {
        User user = userRepository.findById(id).orElseThrow(()-> new NotFoundException(id));
        
        userRepository.delete(user);
        
        //Tady nevim jak to pekne spojit s throw, od java 9 je na to funkce
        //userRepository.findById(id).ifPresent(user -> userRepository.delete(user));
    }
    
}
