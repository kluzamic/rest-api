/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.enehano.training.demoapp.restapi.service;

/**
 *
 * @author Kluzamic
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(Long id){
        super("User not found, id: " + id);
    }
}
